import sys
from datetime import datetime

from CieLabTranslator import rgb2cielab
from FilterProvider import FilterProvider
from GaborFilterProvider import gabor_function
from ImageEdgeDetector import ImageEdgeDetector
from NewNetwork import NewNetwork
import numpy as np
import cv2

if __name__ == '__main__':
    image = cv2.imread('input/01959.jpg', 0)
    # image = rgb2cielab(image)
    image = 1 - image / np.max(image)
    #
    # cv2.imwrite('image/{}.png'.format(datetime.utcnow()), image * 255)
    # sys.exit(0)

    image_size = 13
    lambda_parameter = 3
    filter_function = gabor_function

    filter_provider = FilterProvider(image_size, lambda_parameter, filter_function)
    network = NewNetwork(13, 10, filter_provider)

    print("Network created")
    print("degree_pairs_list, shape = {}".format(len(network.degree_pairs_list)))
    print(network.degree_pairs_list)

    print("W1, shape = {}".format(network.w1.shape))
    print(network.w1)

    print("W2, shape = {}".format(network.w2.shape))
    print(network.w2)

    print("X, shape = {}".format(len(network.x)))

    print("Y, shape = {}".format(len(network.y)))

    accuracy, _ = network.train(epoch_count=50)
    print(accuracy)

    # np.set_printoptions(threshold=np.nan)
    # print(network.w2)

    detector = ImageEdgeDetector(network)
    result_image = detector.new_detect_edges(image)

    cv2.imwrite('image/{}.png'.format(datetime.utcnow()), result_image * 255)

    # answer, value_index, vector, value = network.apply(image)
    # print(answer)
    # print(vector)
    # image = generate_image(image_size, 30, 210, 0, 1)
    # image = np.full((image_size, image_size), 0)
    # answer, value, vector, value_index = network.apply(image)

    # print("degree_pair {} with_value={} at {}, mean = {}, std = {}".format(answer, value, value_index, np.mean(vector), np.std(vector)))
    # print(vector)

