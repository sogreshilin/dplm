import sys

from CieLabTranslator import rgb2cielab
from FilterProvider import FilterProvider
from GaborFilterProvider import gabor_function
from ImageEdgeDetector import ImageEdgeDetector
from Network import Network
import cv2
import numpy as np

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Image filename expected')
        sys.exit(-1)

    FILTER_SIZE = 13
    DEGREE_DELTA = 10
    LAMBDA = 3
    FILTER_FUNCTION = gabor_function
    FILTER_PROVIDER = FilterProvider(FILTER_SIZE, LAMBDA, FILTER_FUNCTION)
    IMAGE_FILENAME = sys.argv[1]
    N_NOISE = 5
    network = Network(FILTER_SIZE, DEGREE_DELTA, FILTER_PROVIDER)

    detector = ImageEdgeDetector(network)
    image = cv2.imread('input/' + IMAGE_FILENAME)
    # print('Converting image to CIE LAB')
    image = rgb2cielab(image)
    image = image / np.max(image)

    # plt.imshow(image, cmap='gray')
    # plt.show()

    # print('Training the network')

    print('''
    <!DOCTYPE html>
    <html lang="en" dir="ltr">
        <head>
            <meta charset="utf-8">
            <title>01959.jpg</title>
            <style>
                table td {
                    min-width: 100px;
                    text-align: center;
                }
    
                table {
                    border-collapse: collapse;
                }
    
                table, th, td {
                    border: 1px solid black;
                }
            </style>
        </head>
        <body>
    ''')

    print('<table>')
    print('<thead>')
    print('<tr>')
    print('<td>Filter center</td>')
    print('<td>Image</td>')
    for degree_pair in network.degree_pairs_list:
        print('<td>{}</td>'.format(degree_pair))
    print('</tr>')
    print('</thead>')

    network.w2 = np.load('trained_w2/w2_{}_{}_{}.npy'.format(FILTER_SIZE, DEGREE_DELTA, FILTER_FUNCTION.__name__))
    # print('ACCURACY', network.accuracy())

    # print('Detecting the edges')
    # noise_degree_pairs = detector.noise_degree_pairs(image, n=N_NOISE)
    print('<tbody>')
    edges = detector.detect_edges(image, [])
    print('</tbody>')
    print('</body>')
    print('</html>')

    edges = (1 - edges) * 255

    # plt.imshow(edges, cmap='gray')
    # plt.show()
    print('</table>')

    cv2.imwrite(
        'output/{}-sigmoid-filter={}-n={}.png'.format(IMAGE_FILENAME.split('.')[0], FILTER_FUNCTION.__name__, N_NOISE),
        edges)
