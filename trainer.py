from FilterProvider import FilterProvider
from GaborFilterProvider import gabor_function
from MexicanHatFilterProvider import mexican_hat_function
from Network import Network
import numpy as np

if __name__ == '__main__':
    LAMBDA = 3
    EPOCH_COUNT = 200

    for FILTER_SIZE in range(9, 23, 2):
        for DEGREE_DELTA in (10, 20, 30):
            for FILTER_FUNCTION in (mexican_hat_function, gabor_function):
                FILTER_PROVIDER = FilterProvider(FILTER_SIZE, LAMBDA, FILTER_FUNCTION)
                network = Network(FILTER_SIZE, DEGREE_DELTA, FILTER_PROVIDER)
                accuracy, _ = network.train(epoch_count=EPOCH_COUNT)
                filename = 'trained_w2/sigmoid_w2_{}_{}_{}.npy'.format(FILTER_SIZE, DEGREE_DELTA, FILTER_FUNCTION.__name__)
                np.save(filename, network.w2)
                print(filename, 'saved', accuracy)
