from CieLabTranslator import rgb2cielab
from ImageEdgeDetector import ImageEdgeDetector
from ImageProvider import generate_image
import cv2
import numpy as np


def save_image():
    image_size = 13
    alpha = 0
    beta = 90
    background = 0.9
    border = 0
    image = generate_image(image_size, alpha, beta, background, border)
    cv2.imwrite('image/({},{})({},{}).png'.format(alpha, beta, background, border), image * 255)


