from matplotlib import pyplot as plt
import cv2 as cv

from ImageProvider import generate_image

if __name__ == '__main__':
    plt.tick_params(
        axis='both',
        which='both',
        bottom=False,
        top=False,
        left=False,
        right=False,
        labelleft=False,
        labelbottom=False)

    for alpha in range(0, 360, 10):
        for beta in range(alpha + 10, 360, 10):
            image = generate_image(13, alpha, beta) * 255
            cv.imwrite('training/{}-{}.png'.format(alpha, beta), image)