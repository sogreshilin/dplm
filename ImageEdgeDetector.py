import numpy as np

from FilterProvider import FilterProvider
from ImageProvider import generate_image
import cv2

from MexicanHatFilterProvider import mexican_hat_function


class ImageEdgeDetector:

    def __init__(self, network):
        """

        :param network: обученная сеть
        """
        self.network = network

    def noise_degree_pairs(self, image, n=5):
        image_height, image_width = image.shape
        filter_size = self.network.image_size

        degree_pairs = {}

        for j in range(0, image_height - filter_size, 1):
            for i in range(0, image_width - filter_size, 1):
                sub_image = image[j:j + filter_size, i:i + filter_size]
                degree_pair, value = self.network.apply(sub_image)
                if degree_pair in degree_pairs:
                    degree_pairs[degree_pair] += 1
                else:
                    degree_pairs[degree_pair] = 1

        sorted_degree_pairs = list(sorted(degree_pairs.items(), key=lambda kv: kv[1]))
        noise_degree_pairs = []
        for k, v in sorted_degree_pairs[-n:]:
            noise_degree_pairs.append(k)
        return noise_degree_pairs

    def detect_edges(self, image):
        image_height, image_width = image.shape
        edges = np.zeros(image.shape)
        filter_size = self.network.image_size

        # for j in range(0, image_height - filter_size, 10):
        # print('{:.2f}%'.format(j * 100 / image.shape[0]))
        # if j > 100:
        #     break
        for a, b in self.network.degree_pairs_list:
            print('<tr>')
            sub_image = generate_image(13, a, b)  # image[j:j + filter_size, i:i + filter_size]
            name = '{}'.format((a, b))
            cv2.imwrite('table/{}.png'.format(name), sub_image * 255)
            print('<td>{}</td>'.format(name))
            print('<td><img src="table/{}.png" alt="{}"></td>'.format(name, name))
            degree_pair, max, a2, arg_max = self.network.apply(sub_image)

            for k, value in enumerate(a2):
                if k == arg_max:
                    print('<td style="background-color: orange;">{0:.4f}</td>'.format(value))
                else:
                    print('<td>{0:.4f}</td>'.format(value))
            print('</tr>')

            # if degree_pair not in filter:
            # edges[j:j + filter_size, i:i + filter_size] += generate_image(filter_size, *degree_pair)

        return np.array([])  # edges / np.max(edges)

    def new_detect_edges(self, image):
        image_height, image_width = image.shape
        edges = np.zeros(image.shape)
        filter_size = self.network.image_size

        stds = []

        for j in range(0, image_height - filter_size, 1):
            for i in range(0, image_width - filter_size, 1):
                sub_image = image[j:j + filter_size, i:i + filter_size]
                degree_pair, value, vector, value_index = self.network.apply(sub_image)
                stds.append(np.std(vector))

        print("mean stds:", np.mean(stds))
        mean_std = np.mean(stds)

        for j in range(0, image_height - filter_size, 1):
            for i in range(0, image_width - filter_size, 1):
                sub_image = image[j:j + filter_size, i:i + filter_size]
                degree_pair, value, vector, value_index = self.network.apply(sub_image)

                if np.std(vector) < mean_std * 0.9:
                    continue
                alpha, beta = degree_pair
                image_angle = generate_image(13, alpha, beta, 0, 1)
                edges[j:j + filter_size, i:i + filter_size] += image_angle

        return 1 - edges / np.max(edges)
