import numpy as np
import math


def mexican_hat_function(lambda_parameter, x, y):
    c1 = 1
    c2 = 1 / 2
    c3 = 2 * math.pi
    u = 1 / lambda_parameter
    v = 0
    f = math.sqrt(u ** 2 + v ** 2)
    gamma = 0.01

    sigma = 0.71

    return np.exp(-(x ** 2 + gamma ** 2 * y ** 2) / (2 * sigma ** 2)) \
           * (1 - (x ** 2 + gamma ** 2 * y ** 2) / (sigma ** 2))
