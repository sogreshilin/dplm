import numpy as np
import math

from AngleTranslator import deg_to_rad
from CoordinateCalculator import compute_center_coordinates, compute_end_coordinates


class FilterProvider:
    def __init__(self, image_size, lambda_parameter, filter_function):
        self.image_size = image_size
        self.lambda_parameter = lambda_parameter
        self.filter_function = filter_function

    def generate_filter(self, angle_deg):
        filter_size = round(self.image_size / 2)

        result = np.zeros((self.image_size, self.image_size))
        x_center, y_center = compute_center_coordinates(self.image_size, angle_deg)

        half_size = filter_size // 2
        for x in range(-half_size, half_size + 1):
            for y in range(-half_size, half_size + 1):
                x_rotated, y_rotated = rotate(x, y, angle_deg)
                try:
                    result[x_center + x, y_center + y] = self.filter_function(self.lambda_parameter, x_rotated, y_rotated)
                except IndexError as e:
                    pass

        return result


def rotate(x, y, angle_deg):
    angle_rad = deg_to_rad(angle_deg)
    x_rotated = x * math.cos(angle_rad) - y * math.sin(angle_rad)
    y_rotated = x * math.sin(angle_rad) + y * math.cos(angle_rad)
    return x_rotated, y_rotated
