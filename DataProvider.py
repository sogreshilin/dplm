import numpy as np
from ImageProvider import generate_image


def create_e_vector(size, index):
    v = np.zeros(size)
    v[index] = 1
    return v


def prepare_data(image_size, degree_delta):
    degrees_list = list(range(0, 360, degree_delta))
    degree_pairs_list = [(a, b) for a in degrees_list for b in range(a + degree_delta, 360, degree_delta)]
    x = []
    y = []
    for background_color in (0., 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 0.6):
        for color in (0.8, 0.9, 1.):
            if background_color != color:
                x.extend([generate_image(image_size, alpha, beta, background_color, color) for alpha, beta in degree_pairs_list])
                y.extend([create_e_vector(len(degree_pairs_list), i) for i in (range(len(degree_pairs_list)))])
    return x, y
