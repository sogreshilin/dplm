import numpy as np
import math

LEARNING_RATE = 0.01


def cos_measure(a, b):
    return math.sqrt(np.sum((a - b) ** 2))
    # return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def left_error(beta, theta,
               a_alpha, w_from_alpha_to_alpha_beta,
               a_beta, w_from_beta_to_alpha_beta,
               a_theta, w_from_theta_to_beta_theta, w_from_beta_to_beta_theta,
               a_expected, a_actual):
    """
    Правильный ответ - пара углов (alpha, beta)
    Около этого ответа - пары (theta, beta), где theta = alpha + delta

    """
    vector_a = np.array((a_alpha * w_from_alpha_to_alpha_beta, a_beta * w_from_beta_to_alpha_beta))
    vector_b = np.array((a_theta * w_from_theta_to_beta_theta, a_beta * w_from_beta_to_beta_theta))
    return LEARNING_RATE * np.exp(-cos_measure(vector_a, vector_b)) * math.cos(beta - theta) \
           * (a_actual - a_expected)


def right_error(alpha, theta,
                a_alpha, w_from_alpha_to_alpha_beta,
                a_beta, w_from_beta_to_alpha_beta,
                a_theta, w_from_theta_to_alpha_theta, w_from_alpha_to_alpha_theta,
                a_expected, a_actual):
    """
    Правильный ответ - пара углов (alpha, beta)
    Около этого ответа - пары (alpha, theta), где theta = beta + delta

    """
    vector_a = np.array((a_alpha * w_from_alpha_to_alpha_beta, a_beta * w_from_beta_to_alpha_beta))
    vector_b = np.array((a_theta * w_from_theta_to_alpha_theta, a_alpha * w_from_alpha_to_alpha_theta))
    return LEARNING_RATE * np.exp(-cos_measure(vector_a, vector_b)) * math.cos(alpha - theta) * (a_actual - a_expected)
