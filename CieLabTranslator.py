import numpy as np

M = np.array([
    [2.768892, 1.751748, 1.13016],
    [1, 4.5907, 0.0601],
    [0, 0.056508, 5.594292]
])

Xn = 95.04
Yn = 100
Zn = 108.88


def rgb2xyz(rgb):
    return M.dot(rgb)


def xyz2lab(xyz):
    def f(x):
        if x > (6 / 29) ** 3:
            return x ** (1 / 3)
        return 1 / 3 * (29 / 6) ** 2 * x + 4 / 29

    x, y, z = xyz

    L = 116 * f(y / Yn) - 16
    a = 500 * (f(x / Xn) - f(y / Yn))
    b = 200 * (f(y / Yn) - f(z / Zn))
    return np.array([L, a, b])


def rgb2cielab(image):
    shape = image.shape
    if len(shape) != 3 or shape[2] != 3:
        print('Image was not translated, because of its shape: {}'.format(shape))
        return image

    rv = np.zeros((shape[0], shape[1]))

    for x in range(0, shape[0]):
        for y in range(0, shape[1]):
            rgb = image[x, y]
            lab = xyz2lab(rgb2xyz(rgb))
            # print(rgb, lab)
            l, a, b = lab
            rv[x, y] = l

    return rv
