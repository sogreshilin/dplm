import math

from AngleTranslator import deg_to_rad


def compute_end_coordinates(image_size, angle):
    a = math.floor(image_size / 2)
    angle = angle % 360
    x1, y1 = 0, 0

    if 0 <= angle < 45:
        alpha_rad = deg_to_rad(angle)
        x1 = a - a * math.tan(alpha_rad)
        y1 = 0
    elif 45 <= angle < 90:
        alpha_rad = deg_to_rad(90 - angle)
        x1 = 0
        y1 = a - a * math.tan(alpha_rad)
    elif 90 <= angle < 135:
        alpha_rad = deg_to_rad(angle - 90)
        x1 = 0
        y1 = a + a * math.tan(alpha_rad)
    elif 135 <= angle < 180:
        alpha_rad = deg_to_rad(180 - angle)
        x1 = a - a * math.tan(alpha_rad)
        y1 = image_size
    elif 180 <= angle < 225:
        alpha_rad = deg_to_rad(angle - 180)
        x1 = a + a * math.tan(alpha_rad)
        y1 = image_size
    elif 225 <= angle < 270:
        alpha_rad = deg_to_rad(270 - angle)
        x1 = image_size
        y1 = a + a * math.tan(alpha_rad)
    elif 270 <= angle < 315:
        alpha_rad = deg_to_rad(angle - 270)
        x1 = image_size
        y1 = a - a * math.tan(alpha_rad)
    elif 315 <= angle < 360:
        alpha_rad = deg_to_rad(360 - angle)
        x1 = a + a * math.tan(alpha_rad)
        y1 = 0
    if x1 == image_size:
        x1 -= 1
    if y1 == image_size:
        y1 -= 1
    return x1, y1


def compute_center_coordinates(image_size, angle):
    x0 = image_size / 2
    y0 = image_size / 2
    x1, y1 = compute_end_coordinates(image_size, angle)
    return round((x0 + x1) / 2), round((y0 + y1) / 2)
