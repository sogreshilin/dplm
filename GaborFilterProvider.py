import math


def gabor_function(lambda_parameter, x, y):
    c1 = 1
    c2 = 1 / 2
    c3 = 2 * math.pi
    u = 1 / lambda_parameter
    v = 0
    f = math.sqrt(u ** 2 + v ** 2)
    gamma = 0.1

    sigma_x = 1.2
    sigma_y = 1.2 / gamma

    return c1 * math.exp(-c2 * (x ** 2 / sigma_x ** 2 + y ** 2 / sigma_y ** 2)) \
           * math.cos(c3 * (u * x + v * y) + 2 * math.pi)

