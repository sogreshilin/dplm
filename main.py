import numpy as np

from GaborFilterProvider import gabor_function
from MexicanHatFilterProvider import mexican_hat_function
from FilterProvider import FilterProvider
from Network import Network

if __name__ == "__main__":
    image_size_range = list(range(9, 22, 2))
    np.set_printoptions(precision=2)
    for filter_function, lambda_parameter in ((gabor_function, 3), (gabor_function, 2), (mexican_hat_function, 3)):
        print("## lambda={}, filter={}, backpropagation".format(lambda_parameter, filter_function.__name__))
        print('dT', *image_size_range, sep="\t")
        for degree_delta in (30, 20, 10):
            accuracies = []
            print(degree_delta, end="\t")
            for image_size in image_size_range:
                filter_provider = FilterProvider(image_size, lambda_parameter, filter_function)
                network = Network(image_size, degree_delta, filter_provider, False)
                acc_0 = network.accuracy()
                acc_max, errors = network.train(epoch_count=300)
                # print(acc_max)
                acc_n = network.accuracy()
                accuracies.append(acc_n)
            print(np.array2string(np.array(accuracies) * 100, separator="\t", precision=2)[1:-2])
        print()
