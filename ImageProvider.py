import math

import numpy as np
from skimage.draw import line, line_aa

from CoordinateCalculator import compute_end_coordinates


def generate_image(image_size, alpha, beta, background_color=0.0, border_color=1.0):
    img = np.full((image_size, image_size), np.float(background_color))
    x0, y0 = math.floor(image_size / 2), math.floor(image_size / 2)
    x1, y1 = compute_end_coordinates(image_size, alpha)
    x2, y2 = compute_end_coordinates(image_size, beta)

    rr1, cc1 = line(round(x0), round(y0), round(x1), round(y1))
    img[rr1, cc1] = np.float(border_color)

    rr2, cc2 = line(round(x0), round(y0), round(x2), round(y2))
    img[rr2, cc2] = np.float(border_color)

    return img
