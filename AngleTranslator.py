import math


def rad_to_deg(angle_rad):
    return 180 * angle_rad / math.pi


def deg_to_rad(angle_deg):
    return math.pi * angle_deg / 180
