import numpy as np

from CosLearning import left_error, right_error
from DataProvider import prepare_data, create_e_vector
from ImageProvider import generate_image


class Network:
    def __init__(self, image_size, degree_delta, filter_provider, train_all_weights=False):
        self.image_size = image_size
        self.degree_delta = degree_delta
        self.train_all_weights = train_all_weights
        self.degrees_list = list(range(0, 360, degree_delta))
        self.degree_pairs_list = [(a, b)
                                  for a in self.degrees_list
                                  for b in range(a + degree_delta, 360, degree_delta)]
        self.x, self.y = prepare_data(image_size, degree_delta)
        self.filter_provider = filter_provider
        self.w1 = self.create_w1()
        self.w2 = self.create_w2()

    def create_w1(self):
        height = len(self.degrees_list)
        width = self.image_size * self.image_size
        result = np.zeros((height, width))
        for i, deg in enumerate(self.degrees_list):
            result[i] = self.filter_provider.generate_filter(deg).reshape(width)
        return result

    def create_w2(self):
        height = len(self.degree_pairs_list)
        width = len(self.degrees_list)

        result = np.zeros((height, width))
        ones_coordinates = list()
        for i in range(0, width):
            for j in range(i + 1, width):
                ones_coordinates.append((i, j))
        for index, coordinates in enumerate(ones_coordinates):
            i, j = coordinates
            result[index, i] = 1
            result[index, j] = 1
        return result

    @staticmethod
    def activation_function(x):
        return x
        # return 1 / (1 + np.exp(-0.025 * x))

    @staticmethod
    def activation_function_derivative(x):
        return np.ones(x.shape)
        # return Network.activation_function(x) * (1 - Network.activation_function(x))
        # return 0.025 * np.exp(-x) / ((1 + 0.025 * np.exp(-x)) ** 2)

    @staticmethod
    def cost_function_x(expected, actual):
        return np.sum((expected - actual) ** 2) / 2
        # return -np.sum(expected * np.log(actual) + (1 - expected) * np.log(1 - actual))

    @staticmethod
    def cost_function_x_derivative(expected, actual):
        return (actual - expected)

    def train(self, epoch_count=100, eta=2.5):
        mask = self.create_w2()
        accuracies = []
        errors = []

        for epoch in range(epoch_count):
            hit_count = 0

            delta_w2 = np.zeros(self.w2.shape)

            b_count = 0
            l_count = 0
            r_count = 0
            for x, y in zip(self.x, self.y):

                a0 = x.reshape((-1))

                z1 = self.w1.dot(a0)
                a1 = self.activation_function(z1)

                z2 = self.w2.dot(a1)
                a2 = self.activation_function(z2)

                expected_index = np.argmax(y)
                actual_index = np.argmax(a2)
                # print('image:', self.degree_pairs_list[expected_index])
                # print(x)
                # print(np.max(a2), np.argmax(a2))


                if expected_index == actual_index:
                    hit_count += 1
                    # if epoch == epoch_count - 1:
                    #     print(" OK", self.degree_pairs_list[expected_index], self.degree_pairs_list[actual_index])
                else:
                    if epoch == epoch_count - 1:
                        # print("!OK", self.degree_pairs_list[expected_index], self.degree_pairs_list[actual_index])
                        e = self.degree_pairs_list[expected_index]
                        a = self.degree_pairs_list[actual_index]

                        if e[0] != a[0] and e[1] != a[1]:
                            #     print("B", e, a)
                            b_count += 1
                        elif e[0] != a[0]:
                            #     print("L", e, a)
                            l_count += 1
                        elif e[1] != a[1]:
                            #     print("R", e, a)
                            r_count += 1
                        errors.append((e, a))

                e = self.degree_pairs_list[expected_index]
                a = self.degree_pairs_list[actual_index]

                delta = self.cost_function_x_derivative(y, create_e_vector(len(a2), np.argmax(a2))) * \
                        self.activation_function_derivative(z2)

                delta_w2 += delta.reshape(-1, 1).dot(a1.reshape(1, -1))

            self.w2 -= eta / len(self.x) * delta_w2

            if not self.train_all_weights:
                self.w2 *= mask

            accuracies.append(hit_count / len(self.x))
            # print('epoch = {} accuracy ={}'.format(epoch, hit_count / len(self.x)))

            # if epoch == epoch_count - 1:
            #     print('errors at coordinates: left={}, right={}, both={}'.format(l_count, r_count, b_count))
        return max(accuracies), errors

    def apply(self, image):
        a0 = image.reshape((-1))

        z1 = self.w1.dot(a0)
        a1 = self.activation_function(z1)

        z2 = self.w2.dot(a1)
        a2 = self.activation_function(z2)

        return self.degree_pairs_list[np.argmax(a2)], np.max(a2), a2, np.argmax(a2)

    def accuracy(self):
        hit_count = 0
        for x, y in zip(self.x, self.y):
            a0 = x.reshape((-1))

            z1 = self.w1.dot(a0)
            a1 = self.activation_function(z1)

            z2 = self.w2.dot(a1)
            a2 = self.activation_function(z2)

            expected_index = np.argmax(y)
            actual_index = np.argmax(a2)

            if expected_index == actual_index:
                hit_count += 1
        return hit_count / len(self.x)

    def errors(self):
        errors = []
        for x, y in zip(self.x, self.y):
            a0 = x.reshape((-1))

            z1 = self.w1.dot(a0)
            a1 = self.activation_function(z1)

            z2 = self.w2.dot(a1)
            a2 = self.activation_function(z2)

            expected_index = np.argmax(y)
            actual_index = np.argmax(a2)

            if expected_index != actual_index:
                e = self.degree_pairs_list[expected_index]
                a = self.degree_pairs_list[actual_index]
                errors.append((e, a))
        return errors

    def cos_train(self, epoch_count=100):
        accuracies = [self.accuracy()]
        # print(accuracies[-1])
        for epoch in range(epoch_count):

            for expected, actual in self.errors():
                # print('error at {}, actual = {}'.format(expected, actual))
                self.cos_train_at(expected)
            accuracies.append(self.accuracy())
            if (accuracies[-1] <= accuracies[-2] or accuracies[-1] == 1):
                break
            # print('epoch={} accuracy = {}'.format(epoch + 1, accuracies[-1]))
        return max(accuracies), self.errors()
            # print(self.a2((0, 80)))
            # for degree_pair, weight_pair in self.weights().items():
            #     print('{}: {}'.format(degree_pair, weight_pair))

    def a1(self, input):
        a0 = generate_image(self.image_size, *input).reshape((-1))
        z1 = self.w1.dot(a0)
        return self.activation_function(z1)

    def a2(self, input):
        a1 = self.a1(input)
        z2 = self.w2.dot(a1)
        a2 = self.activation_function(z2)
        return a2

    def cos_train_at(self, expected):
        w2_delta = np.zeros(self.w2.shape)
        self.cos_train_x_neighbors(expected, w2_delta)
        self.cos_train_y_neighbors(expected, w2_delta)
        self.w2 += w2_delta

    def x_neighbors(self, degree_pair):
        alpha = degree_pair[0]
        beta = degree_pair[1]
        for delta in range(0, 360, self.degree_delta):
            new_alpha = (alpha + delta) % 360
            if new_alpha != beta and (new_alpha, beta) != degree_pair:
                yield new_alpha, beta

    def y_neighbors(self, degree_pair):
        alpha = degree_pair[0]
        beta = degree_pair[1]
        for delta in range(0, 360, self.degree_delta):
            new_beta = (beta + delta) % 360
            if alpha != new_beta and (alpha, new_beta) != degree_pair:
                yield alpha, new_beta

    def cos_train_x_neighbors(self, expected, w2):
        for neighbor in self.x_neighbors(expected):
            alpha = expected[0]
            beta = expected[1]
            theta = neighbor[0]

            index_alpha = self.degrees_list.index(alpha)
            index_beta = self.degrees_list.index(beta)
            index_theta = self.degrees_list.index(theta)

            alpha_beta = tuple(sorted((alpha, beta)))
            alpha_theta = tuple(sorted((alpha, theta)))
            beta_theta = tuple(sorted((theta, beta)))

            index_alpha_beta = self.degree_pairs_list.index(alpha_beta)
            index_alpha_theta = self.degree_pairs_list.index(alpha_theta)
            index_beta_theta = self.degree_pairs_list.index(beta_theta)

            a0 = generate_image(self.image_size, *neighbor).reshape((-1))
            z1 = self.w1.dot(a0)
            a1 = self.activation_function(z1)
            z2 = self.w2.dot(a1)
            a2 = self.activation_function(z2)

            curr_delta_w = left_error(alpha, theta,
                                      a1[index_alpha], self.w2[index_alpha_beta, index_alpha],
                                      a1[index_beta], self.w2[index_alpha_beta, index_beta],
                                      a1[index_theta], self.w2[index_beta_theta, index_theta],
                                      self.w2[index_beta_theta, index_beta],
                                      a2[index_alpha_beta], np.max(a2))

            # print('{}, x-neighbor {}, delta_w = {}'.format(expected, neighbor, curr_delta_w))
            w2[index_alpha_beta, index_alpha] += curr_delta_w

    def cos_train_y_neighbors(self, expected, w2):
        for neighbor in self.y_neighbors(expected):
            alpha = expected[0]
            beta = expected[1]
            theta = neighbor[1]

            index_alpha = self.degrees_list.index(alpha)
            index_beta = self.degrees_list.index(beta)
            index_theta = self.degrees_list.index(theta)

            alpha_beta = tuple(sorted((alpha, beta)))
            alpha_theta = tuple(sorted((alpha, theta)))
            beta_theta = tuple(sorted((theta, beta)))

            index_alpha_beta = self.degree_pairs_list.index(alpha_beta)
            index_alpha_theta = self.degree_pairs_list.index(alpha_theta)
            index_beta_theta = self.degree_pairs_list.index(beta_theta)

            a0 = generate_image(self.image_size, *neighbor).reshape((-1))
            z1 = self.w1.dot(a0)
            a1 = self.activation_function(z1)
            z2 = self.w2.dot(a1)
            a2 = self.activation_function(z2)

            curr_delta_w = right_error(beta, theta,
                                       a1[index_alpha], self.w2[index_alpha_beta, index_alpha],
                                       a1[index_beta], self.w2[index_alpha_beta, index_beta],
                                       a1[index_theta], self.w2[index_alpha_theta, index_theta],
                                       self.w2[index_alpha_theta, index_alpha],
                                       a2[index_alpha_beta], np.max(a2))

            w2[index_alpha_beta, index_beta] += curr_delta_w
            # print('{}, y-neighbor {}, delta_w = {}'.format(expected, neighbor, curr_delta_w))

    def weights(self):
        res = dict()
        for i, degree_pair in enumerate(self.degree_pairs_list):
            a, b = degree_pair
            index_a = self.degrees_list.index(a)
            index_b = self.degrees_list.index(b)
            res[degree_pair] = (self.w2[i, index_a], self.w2[i, index_b])
        return res
